import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InputhandlerTest {

    Inputhandler input = new Inputhandler();

    @Test
    void testDivideInput(){
        input.setNumber("123");
        String[] divided = input.divideInput();
        assertArrayEquals(divided,new String[]{"12", "3"} );
    }
}