import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnTest {

    Luhn luhn = new Luhn();

    @Test
    void testNumberOfDigits(){
        assertFalse(luhn.canBeCreditCard("1234"));
        assertTrue(luhn.canBeCreditCard("1234567891234567"));
    }

    @Test
    void testCheckSumCalculator(){
        assert(luhn.calculateCheckSum("79927398713") == 67);
        assert(luhn.calculateCheckSum("1234567891234567") == 67);
    }

    @Test
    void testCheckDigitCalculator(){
        assertTrue(luhn.calculateCheckDigit("79927398713") == 3);
        assertFalse(luhn.calculateCheckDigit("9999999") == 1);
    }

    @Test
    void testIsValid(){
        assertTrue(luhn.isValid("79927398713"));
        assertFalse(luhn.isValid("789"));
    }

}