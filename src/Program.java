import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Inputhandler input = new Inputhandler();
        Luhn luhn = new Luhn();

        input.getInput();
        String[] dividedInput = input.divideInput();
        System.out.println("Input: " + dividedInput[0] + " " + dividedInput[1]);
        System.out.println("Provided checkdigit: " + dividedInput[1]);
        int calculatedSum = luhn.calculateCheckSum(input.getNumber());
        System.out.println("Calculated checksum: " + calculatedSum);
        int checkDigit = luhn.calculateCheckDigit(input.getNumber());
        System.out.println("caculated check digit: " + checkDigit);
        if(luhn.isValid(input.getNumber())){
            System.out.println("Valid checksum");
        } else {
            System.out.println("Invalid checksum");
        }
    }
}
