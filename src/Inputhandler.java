import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Inputhandler {

    String number;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void getInput(){
        // Ask for input and store if ok
        try{
            String inputNumber = "";
            while(!inputNumber.matches("[0-9]+")) {
                System.out.println("Type a number: ");
                Scanner scan = new Scanner(System.in);
                inputNumber = scan.next();
                setNumber(inputNumber);
                if(!inputNumber.matches("[0-9]+")){
                    System.out.println("Input must be numerical");
                }
            }
        } catch (InputMismatchException i) {
            System.err.println(i.getMessage());
        }
    }

    public String[] divideInput(){
        // Return last digit and rest of number in String array
        String[] dividedInput = new String[2];
        dividedInput[0] = this.number.substring(0, this.number.length() -1);
        dividedInput[1] = this.number.substring(this.number.length() - 1);
        return dividedInput;
    }
}
