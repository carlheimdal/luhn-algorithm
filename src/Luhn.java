public class Luhn {

    public Boolean canBeCreditCard(String number){
        return number.length() == 16;
    }

    public int calculateCheckSum(String number){
        int nDigits = number.length();
        int sum = 0;
        boolean doubleDigit = true;

        for(int i = nDigits - 2; i >= 0; i --){
            int digit = number.charAt(i) - '0';
            if(doubleDigit == true){
                digit *= 2;
                if(digit > 9) digit -= 9;
            }
            sum += digit;
            doubleDigit = !doubleDigit;
        }
        return sum;
    }

    public int calculateCheckDigit(String number){
        int checkSum = calculateCheckSum(number);
        return 10 - (checkSum % 10);
    }

    public boolean isValid(String number){
        return calculateCheckDigit(number) == number.charAt(number.length() -1) - '0';
    }
}
